FROM composer:2.1 AS composer
FROM php:8.0.1-apache

# Not necessary in kube RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

#RUN a2enmod rewrite

RUN apt-get update && apt-get install -y libpq-dev git zip unzip libicu-dev \
    vim curl iputils-ping telnet \
    && docker-php-source extract \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && docker-php-source delete \
    && docker-php-ext-install -j$(nproc) pdo_pgsql intl bcmath sockets \
    && pecl install mongodb \
    && rm -rf /tmp/* \
    && mv /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
    && echo "extension=mongodb.so" >> /usr/local/etc/php/php.ini \
    && sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf \
    && sed -ri -e 's!/var/www/!/var/www/html/public!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

#COPY etc/docker/php/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

#COPY etc/docker/php/my-config.ini /usr/local/etc/php/conf.d/my-config.ini

#one copy

COPY etc/docker/php/* /usr/local/etc/php/conf.d/


# not necesary, we can put all files in var/www/html  ## ENV APACHE_DOCUMENT_ROOT /var/www/orders/public

# not necesary, we can put all files in var/www/html  ## RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf

# not necesary, we can put all files in var/www/html  ## RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf



# NOTE PHP INI  /usr/local/etc/php/ php.ini-development php.ini-production


COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY ./id_rsa* ./config /root/.ssh/

